// hàm dùng để sắp xếp số thứ tự
function sapXepSoThuTu() {
  var a = document.getElementById("inputNum1").value * 1;
  //   console.log("numA: ", numA);
  var b = document.getElementById("inputNum2").value * 1;
  //   console.log("numB: ", numB);
  var c = document.getElementById("inputNum3").value * 1;
  //   console.log("numC: ", numC);
  //   var xepSoThuTu = document.getElementById("txtSortNumber").value;
  var xepSoThuTu;
  // 1
  if (a >= b && b >= c) {
    xepSoThuTu = `${c} < ${b} < ${a}`;
    // 2
  } else if (a >= c && c >= b) {
    xepSoThuTu = `${b} < ${c} < ${a}`;
    // 3
  } else if (b >= a && a >= c) {
    xepSoThuTu = `${c} < ${a} < ${b}`;
    // 4
  } else if (b >= c && c >= a) {
    xepSoThuTu = `${a} < ${c} < ${b}`;
    // 5
  } else if (c >= a && a >= b) {
    xepSoThuTu = `${b} < ${a} < ${c}`;
    // 6
  } else {
    xepSoThuTu = `${a} < ${b} < ${c}`;
  }

  //   console.log("xepSoThuTu: ", xepSoThuTu);
  document.getElementById("txtSortNumber").innerHTML = `${xepSoThuTu}`;
}
// hàm chứa bài 2
function guiLoiChao() {
  // console.log("YES");

  var chonThanhVien = document.getElementById("selUser").value;
  var result;
  if (chonThanhVien == "") {
    // console.log("Xin chào người lạ ơi");
    result = `Xin chào Người lạ ơi!`;
  } else if (chonThanhVien == "B") {
    result = `Xin chào Bố!`;
    // console.log("Chào Bố");
  } else if (chonThanhVien == "M") {
    // console.log("Chào Mẹ");
    result = `Xin chào Mẹ!`;
  } else if (chonThanhVien == "A") {
    // console.log("Chào anh trai");
    result = `Xin chào Anh!`;
  } else {
    // console.log("Chào em gái");
    result = `Xin chào Em gái!`;
  }
  document.getElementById("txtHello").innerHTML = `${result}`;
}

// hàm bài 3
function demSo() {
  var count1 = document.getElementById("inputCount1").value * 1;
  var count2 = document.getElementById("inputCount2").value * 1;
  var count3 = document.getElementById("inputCount3").value * 1;

  var result = 0;

  function kiemtraChanLe(n) {
    // so sánh điều kiện bằng if rồi chạy result
    if (n % 2 == 0) {
      // console.log("số chẵn");
      result = result + 1;
    }
    // toán tử 3 ngôi
    // n % 2 == 0 && result++;
  }
  kiemtraChanLe(count1);
  kiemtraChanLe(count2);
  kiemtraChanLe(count3);
  document.getElementById("txtCount").innerHTML = `Có ${result} số chẵn ,${
    3 - result
  } số lẽ`;
}
//hàm bài 4

function duDoanTamGiac() {
  var edge1 = document.getElementById("inputEdge1").value * 1;
  var edge2 = document.getElementById("inputEdge2").value * 1;
  var edge3 = document.getElementById("inputEdge3").value * 1;

  var a = Math.sqrt(Math.pow(edge2, 2) + Math.pow(edge3, 2));
  var b = Math.sqrt(Math.pow(edge1, 2) + Math.pow(edge3, 2));
  var c = Math.sqrt(Math.pow(edge1, 2) + Math.pow(edge2, 2));
  // var edge1 = 2;
  // var edge2 = 2;
  // var edge3 = 1;
  var result = 0;
  function kiemTraTamGiac() {
    // var a = Math.sqrt(Math.pow(n2, 2) + Math.pow(n3, 2));
    // var b = Math.sqrt(Math.pow(n1, 2) + Math.pow(n3, 2));
    // var c = Math.sqrt(Math.pow(n1, 2) + Math.pow(n2, 2));
    if ((edge1 == edge2) & (edge2 == edge3)) {
      // console.log("Hình tam giác đều");
      result = "Hình tam giác đều";
    } else if (edge1 == edge2 || edge2 == edge3 || edge1 == edge3) {
      // console.log("Hình tam giác cân");
      result = "Hình tam giác cân";
    } else if (edge1 == a || edge2 == b || edge3 == c) {
      // console.log("Hình tam giác vuông");
      result = "Hình tam giác vuông";
    } else {
      result = "Một loại tam giác nào đó";
    }
  }
  kiemTraTamGiac({ edge1, edge2, edge3 });

  document.getElementById("txtEdge").innerHTML = result;
}
